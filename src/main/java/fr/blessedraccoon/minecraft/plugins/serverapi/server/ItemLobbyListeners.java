package fr.blessedraccoon.minecraft.plugins.serverapi.server;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

public final class ItemLobbyListeners implements Listener {

    private ItemStack item;
    private ServerSwitcher sw;
    private Plugin plugin;

    public ItemLobbyListeners(@NotNull Plugin plugin) {
        this.item = new ItemLobby().getItem();
        this.sw = new ServerSwitcher(Server.LOBBY, plugin);
        this.plugin = plugin;
    }

    public boolean isLobbyItem(@NotNull ItemStack item) {
        return item.getItemMeta().equals(item.getItemMeta());
    }

    @EventHandler
    public void onClick(PlayerInteractEvent e) {
        if(e.getHand() == EquipmentSlot.OFF_HAND) return;
        if (this.isLobbyItem(e.getPlayer().getInventory().getItemInMainHand())) {
            this.sw.move(e.getPlayer());
        }
    }

}
