package fr.blessedraccoon.minecraft.plugins.serverapi.commands;

import fr.blessedraccoon.minecraft.plugins.commandmanager.CommandManager;
import fr.blessedraccoon.minecraft.plugins.serverapi.server.Server;
import fr.blessedraccoon.minecraft.plugins.serverapi.server.ServerSwitcher;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.jetbrains.annotations.NotNull;

public class Lobby extends CommandManager {

    private ServerSwitcher sw;

    public Lobby(@NotNull Plugin plugin) {
        this.sw = new ServerSwitcher(Server.LOBBY, plugin);
    }

    @Override
    public String getName() {
        return "lobby";
    }

    @Override
    public String getDescription() {
        return "sends a player to a lobby";
    }

    @Override
    public String getSyntax() {
        return "lobby";
    }

    @Override
    public int getDeepLevel() {
        return 0;
    }

    @Override
    public boolean perform(Player player, String[] strings) {
        return sw.move(player);
    }

    @Override
    public String getPermission() {
        return "lobby";
    }
}
