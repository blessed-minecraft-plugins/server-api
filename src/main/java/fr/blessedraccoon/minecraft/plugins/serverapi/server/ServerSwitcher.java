package fr.blessedraccoon.minecraft.plugins.serverapi.server;

import net.kyori.adventure.text.Component;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.messaging.ChannelNotRegisteredException;
import org.jetbrains.annotations.NotNull;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;

public class ServerSwitcher {

    private Server destination;
    private Plugin plugin;

    public ServerSwitcher(@NotNull Server server, @NotNull Plugin plugin) {
        this.destination = server;
        this.plugin = plugin;
    }

    public boolean move(Player player) {
        ByteArrayOutputStream b = new ByteArrayOutputStream();
        var out = new DataOutputStream(b);
        try {
            out.writeUTF("Connect");
            out.writeUTF(this.destination.getName());
            player.sendPluginMessage(plugin, "BungeeCord", b.toByteArray());
        } catch (Exception e) {
            player.kick(Component.text(""));
            return false;
        }
        return true;
    }

    public Server getDestination() {
        return destination;
    }
}
