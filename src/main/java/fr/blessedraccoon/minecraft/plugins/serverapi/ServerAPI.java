package fr.blessedraccoon.minecraft.plugins.serverapi;

import fr.blessedraccoon.minecraft.plugins.serverapi.commands.Lobby;
import fr.blessedraccoon.minecraft.plugins.serverapi.server.ItemLobbyListeners;
import org.bukkit.plugin.java.JavaPlugin;

public class ServerAPI extends JavaPlugin {

    @Override
    public void onEnable() {
        this.getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");

        var command = getCommand("lobby");
        if (command != null) {
            var commandManager = new Lobby(this);
            command.setExecutor(commandManager);
        }

        var lobbyListener = new ItemLobbyListeners(this);
        this.getServer().getPluginManager().registerEvents(lobbyListener, this);
    }

}
