package fr.blessedraccoon.minecraft.plugins.serverapi.server;

import org.jetbrains.annotations.NotNull;

public enum Server {

    LOBBY("lobby"),
    BALLS_OF_STEEL_1("bos1");

    private String name;

    Server(@NotNull String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
