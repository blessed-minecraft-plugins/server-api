package fr.blessedraccoon.minecraft.plugins.serverapi.server;

import net.kyori.adventure.text.Component;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

public class ItemLobby {

    private ItemStack item;

    public ItemLobby() {
        this.item = new ItemStack(Material.BARRIER);
        var meta = item.getItemMeta();
        meta.displayName(Component.text(ChatColor.RED + "Retourner au lobby " + ChatColor.GRAY + "(Clic Droit)"));
        meta.setUnbreakable(true);
        item.setItemMeta(meta);
    }

    public ItemStack getItem() {
        return item;
    }
}
